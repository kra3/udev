# ------------------------------------------------------------------------------
# Rules for every output format
# ------------------------------------------------------------------------------

DX_FILES =

# --- HTML

if DX_COND_html
DX_FILES += html
endif

# --- CHM (and CHI)

if DX_COND_chm
DX_FILES += chm
if DX_COND_chi
DX_FILES += @PACKAGE@.chi
endif
endif

# --- MAN

if DX_COND_man
DX_FILES += man
endif

# --- RTF

if DX_COND_rtf
DX_FILES += rtf
endif

# --- XML

if DX_COND_xml
DX_FILES += xml
endif

# --- PS

if DX_COND_ps
DX_FILES += @PACKAGE@.ps

DX_PS_GOAL = doxygen-ps

doxygen-ps: @DX_DOCDIR@/@PACKAGE@.ps

@DX_DOCDIR@/@PACKAGE@.ps: @DX_DOCDIR@/@PACKAGE@.tag
	cd @DX_DOCDIR@/latex; \
	rm -f *.aux *.toc *.idx *.ind *.ilg *.log *.out; \
	$(DX_LATEX) refman.tex; \
	$(MAKEINDEX_PATH) refman.idx; \
	$(DX_LATEX) refman.tex; \
	countdown=5; \
	while $(DX_EGREP) 'Rerun (LaTeX|to get cross-references right)' \
	                  refman.log > /dev/null 2>&1 \
	   && test $$countdown -gt 0; do \
	    $(DX_LATEX) refman.tex; \
	    countdown=`expr $$countdown - 1`; \
	done; \
	$(DX_DVIPS) -o ../@PACKAGE@.ps refman.dvi
endif

# --- PDF

if DX_COND_pdf
DX_FILES += @PACKAGE@.pdf

DX_PDF_GOAL = doxygen-pdf

doxygen-pdf: @DX_DOCDIR@/@PACKAGE@.pdf

@DX_DOCDIR@/@PACKAGE@.pdf: @DX_DOCDIR@/@PACKAGE@.tag
	cd @DX_DOCDIR@/latex; \
	rm -f *.aux *.toc *.idx *.ind *.ilg *.log *.out; \
	$(DX_PDFLATEX) refman.tex; \
	$(DX_MAKEINDEX) refman.idx; \
	$(DX_PDFLATEX) refman.tex; \
	countdown=5; \
	while $(DX_EGREP) 'Rerun (LaTeX|to get cross-references right)' \
	                  refman.log > /dev/null 2>&1 \
	   && test $$countdown -gt 0; do \
	    $(DX_PDFLATEX) refman.tex; \
	    countdown=`expr $$countdown - 1`; \
	done; \
	mv refman.pdf ../@PACKAGE@.pdf
endif

# ------------------------------------------------------------------------------
# Targets
# ------------------------------------------------------------------------------

DX_CLEAN = -r \
	@DX_DOCDIR@/html \
	@DX_DOCDIR@/chm \
	@DX_DOCDIR@/@PACKAGE@.chi \
	@DX_DOCDIR@/man \
	@DX_DOCDIR@/rtf \
	@DX_DOCDIR@/xml \
	@DX_DOCDIR@/@PACKAGE@.ps \
	@DX_DOCDIR@/@PACKAGE@.pdf \
	@DX_DOCDIR@/latex

MOSTLYCLEANFILES = $(DX_CLEAN)

@DX_DOCDIR@/@PACKAGE@.tag: $(DX_CONFIG) $(pkginclude_HEADERS)
	rm -rf $(DX_CLEAN)
	$(DX_ENV) $(DX_DOXYGEN) $(srcdir)/$(DX_CONFIG)

doxygen-run: @DX_DOCDIR@/@PACKAGE@.tag
doxygen-doc: doxygen-run $(DX_PS_GOAL) $(DX_PDF_GOAL)

all-local: doxygen-doc

install-data-local:
	mkdir -p $(DESTDIR)$(docdir)/API
	cd $(DX_DOCDIR) && cp -r $(DX_FILES) $(DESTDIR)$(docdir)/API

uninstall-local:
	rm -rf $(DESTDIR)$(docdir)/API

.PHONY: doxygen-run doxygen-doc $(DX_PS_GOAL) $(DX_PDF_GOAL)
